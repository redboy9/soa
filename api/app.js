const express = require('express')
const app = express()
var request = require("request");
var access_token = 'BQBKz5RJemeeFFO6lGaDjl8TVHmSp5AsSbzRXA_ePFcu7xYPl7OUw-Maw0nn8L0rCz9Eu5UKtJRADxzsQO65BPT5sBRHasm3box-63Qn9RTf9u1au7k_ZCDD_hG331Rl4yLdB9_vSCjZSaS2aPZ-wphaBgugWjE9cdClhzs7Ly_9mLF9Dw'
var refresh_token = 'AQBla9r1pCy6qQ4XAJtn18eSC1ezni-ZEsI5yHPzQteHgNdAWDXroVdLm8YhNphKKe7NRUpXPwiTPaFEPApt8iY0meVnxQ-cTYstl0W0YPVzdDTVj00dGIrilFItOxZInuE'
app.use(express.urlencoded({extended:true}))
app.use(express.json());

// list album: 2gertXS08whDTzBWfmewPO,5MmndGNrJgTLd5W7HNmVST,28Orm6S3LezppbfYdgA3aI,6O6woc7tqaZPseZ9IESkNX
app.get('/severalAlbums',async function(req,res) {
	let ID = req.body.listId;
	let idnya = "";
	for(let i= 0;i<ID.length;i++){
		if(i != ID.length -1) idnya +=  ID[i] + ","
		else idnya += ID[i];
	}
	if(idnya.length == 0 || idnya == null || ID == null){
		if(!error)res.status(400).send("Harus ada listId di body");
	}
	else{
		const albumList = await new Promise(function(resolve,reject){
			var options = {
				'method' : 'GET',
				'url' : `https://api.spotify.com/v1/albums?ids=${ID}`,
				'headers' :{
					'Accept': 'application/json',
					'Content-Type':'application/json',
					'Authorization': `Bearer ${access_token}`
				}
			}
			request(options,function(error,response){
				if(error) reject(new Error(error));
				else resolve(JSON.parse(response.body));
			});
		})
		if(albumList.error){
			res.status(albumList.error.status).send(albumList)
			return
		}
		let tempAlbum = [];
		albumList.albums.forEach(album => {
			let albumBaru = {
				"Nama Album": album.name,
				"Produksi": album.label,
				"Total track" : album.total_tracks,
				"Penyanyi" : []
			}
			album.artists.forEach(artis => {
				albumBaru.Penyanyi.push(artis.name);
			});
			tempAlbum.push(albumBaru);
		});
		res.send(tempAlbum);
	
	}
	
	
})

app.get("/detailAlbum/:id",async function (req,res){
	let ID = req.params.id;
	const album = await new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/albums/${ID}`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	})
	if(album.error){
		res.status(album.error.status).send(album)
		return
	}
	let albumBaru = {
		"Nama Album": album.name,
		"Produksi": album.label,
		"Tanggal rilis" : album.release_date,
		"Total track" : album.total_tracks,
		"Tracks" : [],
		"Penyanyi" : []
	}
	album.tracks.items.forEach(track =>{
		albumBaru.Tracks.push({"Nama Lagu": track.name,"Durasi": millisToMinutesAndSeconds(track.duration_ms)})
	})
	album.artists.forEach(artist =>{
		albumBaru.Penyanyi.push(artist.name);
	})
	res.send(albumBaru);
})

// list artis:  0oSGxfWSnnOXhD2fKuz2Gy,3dBVyJ7JuOMt4GE9607Qin,2CIMQHirSU0MQqyYHq0eOx,57dN52uHvrHOxijzpIgu3E,1vCWHaC5f2uS3yhpwWbIA6
app.get("/severalArtist", async function (req,res){
	let ID = req.body.listId;
	let newID = "";
	for (let i = 0; i < ID.length; i++) {
		if(ID[i] == ','){newID += "%2C"}
		else{newID += ID[i]}
	}
	const artist = await new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/artists?ids=${newID}`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	})
	if(artist.error){
		res.status(artist.error.status).send(artist)
		return
	}
	let newList = [];
	artist.artists.forEach(art => {
		let albumBaru = {
			"Nama Artis": art.name,
			"Popularitas": art.popularity,
			"Follower" : art.followers.total,
			"Daftar genre" : art.genres,
		}
		newList.push(albumBaru);
	});
	
	res.send(newList);
})

app.get("/detailArtist/:Id", async function (req,res){
	let ID = req.params.Id;
	const artist = await new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/artists/${ID}`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	})
	if(artist.error){
		res.status(artist.error.status).send(artist)
		return
	}
	let d_artist = {
		"Nama Artis": artist.name,
		"Popularitas": artist.popularity,
		"Follower" : artist.followers.total,
		"Daftar genre" : artist.genres,
	}
	
	res.send(d_artist);
})

app.get('/detailAlbum/:id/tracks', async function(req,res){
	let id = req.params.id
	const track = await (new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/albums/${id}/tracks`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	}))
	if(track.error){
		res.status(track.error.status).send(track)
		return
	}
	let d_track = {
		"Total Track" : track.total,
		"Track" : []
	}
	track.items.forEach(tr => {
		let item = {
			"Nama Track" : tr.name,
			"Jumlah Disc" : tr.disc_number,
			"Popularity" : tr.popularity,
			"Durasi" : millisToMinutesAndSeconds(tr.duration_ms),
			"Penyanyi" : []
		}
		tr.artists.forEach(artist => {
			item.Penyanyi.push({"Nama" : artist.name})
		})
		d_track.Track.push(item)
	});

	res.send(d_track)
})
// list kode country: AD,AE,AR,AT,AU,BE,BG,BH,BO,BR,CA,CH,ES
app.get('/detailArtist/:id/topTrack', async function(req,res){
	if(req.body.country == null || req.body.country == ""){
		res.status(400).send("Harus ada country di body");
		return
	}
	let ID = req.params.id;
	let country = req.body.country;
	const artist = await new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/artists/${ID}/top-tracks?country=${country}`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	})
	if(artist.error){
		res.status(artist.error.status).send(artist)
		return
	}
	let d_track = []
	artist.tracks.forEach(tr => {
		let item = {
			"Nama Track" : tr.name,
			"Nama Album" : tr.album.name,
			"Jumlah Disc" : tr.disc_number,
			"Popularity" : tr.popularity,
			"Durasi" : millisToMinutesAndSeconds(tr.duration_ms),
			"Penyanyi" : []
		}
		tr.artists.forEach(artist => {
			item.Penyanyi.push({"Nama" : artist.name})
		})
		d_track.push(item)
	});

	res.send(d_track)
})

//list track: 7ouMYWpwJ422jRcDASZB7P,4VqPOruhp5EdPBeR92t6lQ,2takcwOaAZWiXQijPHIx7B,11dFghVXANMlKmJXsNCbNl,6Q4PYJtrq8CBx7YCY5IyRN,10Sydb6AAFPdgCzCKOSZuI
app.get('/severalTracks', async function(req,res){
	if(req.body.listId == null || req.body.listId == ""){
		res.status(400).send("Harus ada listId di body");
		return
	}
	let ids = req.body.listId
	const track = await (new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/tracks?ids=${ids}`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	}))
	if(track.error){
		res.status(track.error.status).send(track)
		return
	}
	let d_track = []
	track.tracks.forEach(tr => {
		let item = {
			"Nama Track" : tr.name,
			"Nama Album" : tr.album.name,
			"Jumlah Disc" : tr.disc_number,
			"Popularity" : tr.popularity,
			"Durasi" : millisToMinutesAndSeconds(tr.duration_ms),
			"Penyanyi" : []
		}
		tr.artists.forEach(artist => {
			item.Penyanyi.push({"Nama" : artist.name})
		})
		d_track.push(item)
	});

	res.send(d_track)
})

app.get('/detailTrack/:id', async function(req,res) {
	let id = req.params.id
	const track = await (new Promise(function(resolve,reject){
		var options = {
			'method' : 'GET',
			'url' : `https://api.spotify.com/v1/tracks/${id}`,
			'headers' :{
				'Accept': 'application/json',
				'Content-Type':'application/json',
				'Authorization': `Bearer ${access_token}`
			}
		}
		request(options,function(error,response){
			if(error) reject(new Error(error));
			else resolve(JSON.parse(response.body));
		});
	}))
	if(track.error){
		res.status(track.error.status).send(track)
		return
	}
	let d_track = {
		"Nama Track" : track.name,
		"Nama Album" : track.album.name,
		"Jumlah Disc" : track.disc_number,
		"Popularity" : track.popularity,
		"Durasi" : millisToMinutesAndSeconds(track.duration_ms),
		"Penyanyi" : []
	}
	track.artists.forEach(artist => {
		d_track.Penyanyi.push({"Nama" : artist.name})
	});

	res.send(d_track)
})

function millisToMinutesAndSeconds(millis) {
	var minutes = Math.floor(millis / 60000);
	var seconds = ((millis % 60000) / 1000).toFixed(0);
	return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

app.get("/", (req, res)=>{
	res.send("OK");
})

//https://example.com/?code=AQDHBiNbtMH3OcNQWeKKRk-0phDPXC2Z7Q0ImPB0lEzwZJ0TO8v-4GWOpI5OU5TcmcEFsEcMM8IJw7yLUIZ--LYiXOOs__d2SkE6vqhkFnpMgqNpomEZwaUSwjgE75-ONKOAnmEd7GLG7yjmInuULKh3Kw2rISyADhm0rn2_Gy4SzrFOPfzX8Bi0TxLXhDrxrxDsSnLwuEyoHIbaNgzkTPkjEYsqgIwnrXWvCr0&state=ncf1XD5N3h4Kzhhq
//code=AQDHBiNbtMH3OcNQWeKKRk-0phDPXC2Z7Q0ImPB0lEzwZJ0TO8v-4GWOpI5OU5TcmcEFsEcMM8IJw7yLUIZ--LYiXOOs__d2SkE6vqhkFnpMgqNpomEZwaUSwjgE75-ONKOAnmEd7GLG7yjmInuULKh3Kw2rISyADhm0rn2_Gy4SzrFOPfzX8Bi0TxLXhDrxrxDsSnLwuEyoHIbaNgzkTPkjEYsqgIwnrXWvCr0
//state=ncf1XD5N3h4Kzhhq
//access_token=BQBKz5RJemeeFFO6lGaDjl8TVHmSp5AsSbzRXA_ePFcu7xYPl7OUw-Maw0nn8L0rCz9Eu5UKtJRADxzsQO65BPT5sBRHasm3box-63Qn9RTf9u1au7k_ZCDD_hG331Rl4yLdB9_vSCjZSaS2aPZ-wphaBgugWjE9cdClhzs7Ly_9mLF9Dw
//refresh_token=AQBla9r1pCy6qQ4XAJtn18eSC1ezni-ZEsI5yHPzQteHgNdAWDXroVdLm8YhNphKKe7NRUpXPwiTPaFEPApt8iY0meVnxQ-cTYstl0W0YPVzdDTVj00dGIrilFItOxZInuE

function refresh(){
	// requesting access token from refresh token
	var client_id = 'b34c9e4947344f068a942bee35012d77';
	var client_secret = '666e87dff80f4cfaa45bebce01dc1204';
	var authOptions = {
	  url: 'https://accounts.spotify.com/api/token',
	  headers: { 'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64')) },
	  form: {
		grant_type: 'refresh_token',
		refresh_token: refresh_token
	  },
	  json: true
	};
	request.post(authOptions, function(error, response, body) {
		if (!error && response.statusCode === 200) {
			access_token = body.access_token;
			console.log(access_token)
	  	}else{
			console.log(body)
		}
	});
}
app.listen(3000 || process.env.PORT,() => {
	refresh();
	console.log("Listen to Port 3000...")
})
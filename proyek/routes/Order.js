const express = require('express')
const connection = require("../connection")
const jwt = require('jsonwebtoken')
const multer = require('multer')
const router = express.Router()
const Flight = require("./FlightHelper");
const formatHelper = require("./formatHelper")

const APIKEY_AMADEUS = 'VGxk29GRC8793xnumnaiBzuQh01tVFG5'
const APISECRET_AMADEUS = 'temI5MAfWkdhigKW'

const storage = multer.diskStorage({
    destination : (req,file,callback) => {
        callback(null,'./uploads')
    },
    filename : (req,file,callback) => {
        const mime = file.originalname.split('.')
        callback(null, req.body.username ? req.body.username : file.originalname + '.' + mime[mime.length - 1])
    }
})
const upload = multer({
    storage : storage
})

router.get("/ListOrder", async (req, res) => {
   let check = await checkToken(req.header("x-auth-token"));
   if(check.code != 200){
       return res.status(check.code).send({message : check.message})
   }
   let idCustomer = check.user.id_user;
   let query = `SELECT id_order, tanggal_pemesanan, tanggal_keberangkatan, tanggal_sampai, kota_asal, kota_tujuan, total FROM flight_order WHERE ID_USER = '${idCustomer}'`;
   req.query.id_order ? (query += ` AND ID_ORDER = '${req.query.id_order}'`):(``)
   req.query.min_date ? (query += ` AND Tanggal_pemesanan >= '${(req.query.min_date)}'`):(``)
   req.query.max_date ? (query += ` AND Tanggal_pemesanan <= '${(req.query.max_date)}'`):(``)
   search_h = await connection.query(query, [])
   if(search_h.length > 0){
        for (let i = 0; i < search_h.length; i++) {
            search_h[i].tanggal_pemesanan = formatHelper.timeFormat(search_h[i].tanggal_pemesanan)
            search_h[i].tanggal_keberangkatan = formatHelper.timeFormat(search_h[i].tanggal_keberangkatan)
            search_h[i].tanggal_sampai = formatHelper.timeFormat(search_h[i].tanggal_sampai)
        }
        return res.send(search_h)
   }
   else{
        return res.status(400).send({message : "Tidak ada list order sekarang ini"})
   }
})

router.get("/DetailOrder/:idOrder", async (req, res) => {   
    let idOrder = req.params.idOrder;
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
        return res.status(check.code).send({message : check.message})
    }
    let idCustomer = check.user.id_user;
    let search_h = await connection.query("SELECT * FROM flight_order WHERE id_user = ? and id_order = ?",[idCustomer, idOrder])
    if(search_h.length>0){
        for (let i = 0; i < search_h.length; i++) {
            search_h[i].tanggal_pemesanan = formatHelper.timeFormat(search_h[i].tanggal_pemesanan)
            search_h[i].tanggal_keberangkatan = formatHelper.timeFormat(search_h[i].tanggal_keberangkatan)
            search_h[i].tanggal_sampai = formatHelper.timeFormat(search_h[i].tanggal_sampai)
            let search_d = await connection.query("SELECT gelar, nama, if(tipe=1, 'Dewasa','Bayi') as keterangan FROM d_penumpang WHERE id_order = ?",[idOrder])
            search_h[i].detail = search_d
        }
       return res.send(...search_h)
    }
    else{
        return res.status(400).send({message : "Detail Order dengan id order " + idOrder + " tidak ditemukan"})
    }
 })

 router.put("/Reschedule/:idOrder/:SearchId", async (req, res) => {
    let idOrder = req.params.idOrder
    let SearchId = req.params.SearchId
    let tgl_baru = new Date(req.body.tanggal_baru)
    let check = await checkToken(req.header("x-auth-token"), "reschedule");
    if(check.code != 200){
        return res.status(check.code).send({message : check.message})
    }
    let search_d = await connection.query("SELECT * FROM flight_order WHERE id_user = ? and id_order = ?",[check.user.id_user, idOrder])
    
    if(search_d.length>0){
        let hargaSebelum = search_d[0].total
        let t = await flightSerach(search_d[0].kota_asal, search_d[0].kota_tujuan, req.body.tanggal_baru, undefined, search_d[0].class)
        if(t.length == 0){
            return res.status(400).send({message:"Tidak ada penerbangan tersedia ditanggal tersebut"})
        }
        if (SearchId >= t.length){
            return res.status(400).send({message:"Tidak ada penerbangan tersedia ditanggal tersebut"})
        }
        let flightDetail = t[SearchId]
        let harga = flightDetail.Detail.IDR - hargaSebelum
        let status = 1;
        if (harga <= 0 && search_d[0].status == 3){
            status = 3
        }
        //
        console.log(flightDetail)

        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1);
        let updateFlight = await connection.query("UPDATE FLIGHT_ORDER set TANGGAL_PEMESANAN = ?, CARRIER = ?, AIRCRAFT_CODE = ?, CARRIER_CODE = ?, FLIGHT_NUMBER = ?, DURATION = ?, TANGGAL_KEBERANGKATAN =?, JAM_KEBERANGKATAN =?, TANGGAL_SAMPAI =?, JAM_SAMPAI =?, TOTAL =?, STATUS = ? WHERE id_order = ?", [formatHelper.timeFormat(new Date()), flightDetail.Detail[0].Carrier, flightDetail.Detail[0].Aircraft_code,
            flightDetail.Detail[0].Carrier_code,
            flightDetail.Detail[0].Flight_Number,
            flightDetail.Detail[0].Durasi,
            (flightDetail.Detail[0].Jam_Berangkat.split(" ")[0]),
            (flightDetail.Detail[0].Jam_Berangkat.split(" ")[1]),
            (flightDetail.Detail[0].Jam_Sampai.split(" ")[0]),
            (flightDetail.Detail[0].Jam_Sampai.split(" ")[1]), flightDetail.Detail[0].IDR, status, idOrder])
       
        search_d = await connection.query("SELECT * FROM flight_order WHERE id_order = ?",[idOrder])
        search_d.forEach(element => {
            element.tanggal_pemesanan = formatHelper.timeFormat(element.tanggal_pemesanan)
            element.tanggal_keberangkatan = formatHelper.timeFormat(element.tanggal_keberangkatan)
            element.tanggal_sampai = formatHelper.timeFormat(element.tanggal_sampai)
        });
        search_d[0].message = "Berhasil Reschedule Flight"
        return res.send(...search_d)
    }
    else{
        return res.status(400).send({message : "Detail Order dengan id order " + idOrder + " tidak ditemukan"})
    }
 })

 router.delete("/Cancel/:idOrder", async (req, res) => {
    let tgl_baru = new Date(req.body.tanggal_baru)
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
        return res.status(check.code).send({message : check.message})
    }
    let search_d = await connection.query("SELECT * FROM flight_order WHERE id_user = ? and id_order = ?",[check.user.id_user, req.params.idOrder])
    if(search_d.length>0){
        if(search_d[0].status == -10){
            return res.status(400).send({message : "Cancel order harus order yang aktif"})
        }
        // let delete_penumpang = await connection.query("DELETE FROM d_penumpang WHERE Id_Order = ?",[req.params.idOrder])
        // let delete_order = await connection.query("DELETE FROM FLIGHT_ORDER WHERE Id_Order = ?",[req.params.idOrder])
        let soft_delete_order = await connection.query("UPDATE FLIGHT_ORDER SET STATUS = -10 WHERE Id_Order = ?",[req.params.idOrder])
        return res.send({message : "Berhasil cancel order flight"})
    }
    else{
        return res.status(400).send({message : "Detail Order dengan id order " + req.params.idOrder + " tidak ditemukan"})
    }
 })

 router.post("/AddOrder/:SearchId", async (req, res) => {
    let SearchId = req.params.SearchId
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
        return res.status(check.code).send({message : check.message})
    }

    let t = await flightSerach(req.body.kota_asal, req.body.kota_tujuan, req.body.tanggal_keberangkatan, req.body.tanggal_pulang, req.body.travelClass)
    if(t.length == 0){
        return res.status(400).send({message : "Tidak ada penerbangan ditanggal tersebut dengan kelas " + req.body.travelClass})
    }
    //return res.send(t);
    if(SearchId < t.length){
        
        let flightDetail = t[SearchId]
        let idCustomer = check.user.id_user
        for (let i = 0; i < flightDetail.Detail.length; i++) {
            let idOrder = Math.round((new Date()).getTime() / 1000) + i
            let insertFlight = await connection.query("INSERT INTO FLIGHT_ORDER(ID_ORDER, ID_USER, TANGGAL_PEMESANAN, CARRIER, AIRCRAFT_CODE, CARRIER_CODE, FLIGHT_NUMBER, DURATION, KOTA_ASAL, KOTA_TUJUAN, JUMLAH_PENUMPANG, TANGGAL_KEBERANGKATAN, JAM_KEBERANGKATAN, TANGGAL_SAMPAI, JAM_SAMPAI, CLASS, TOTAL, STATUS) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[idOrder, idCustomer, formatHelper.timeFormat(new Date()), flightDetail.Detail[i].Carrier, flightDetail.Detail[i].Aircraft_code,
            flightDetail.Detail[i].Carrier_code,
            flightDetail.Detail[i].Flight_Number,
            flightDetail.Detail[i].Durasi,  flightDetail.Detail[i].Tempat_Asal,
            flightDetail.Detail[i].Tempat_Destinasi,
            req.body.data_penumpang.length,
            (flightDetail.Detail[i].Jam_Berangkat.split(" ")[0]),
            (flightDetail.Detail[i].Jam_Berangkat.split(" ")[1]),
            (flightDetail.Detail[i].Jam_Sampai.split(" ")[0]),
            (flightDetail.Detail[i].Jam_Sampai.split(" ")[1]), 
            flightDetail.Detail[i].Class,
            flightDetail.Detail[i].IDR, 1])
            console.log(insertFlight)

            for (let j = 0; j < req.body.data_penumpang.length; j++) {
                let insertPenumpang = await connection.query("INSERT INTO d_penumpang (id_order, gelar, nama, tipe, status) values (?,?,?,?,?)", [idOrder, req.body.data_penumpang[j].gelar, req.body.data_penumpang[j].nama, req.body.data_penumpang[j].tipe, 1])
            }
        }
        flightDetail.message = "Berhasil booking flight"
        return res.send(flightDetail)
    }
    else{
        return res.status(400).send({message : "Tidak ada search id " + SearchId})
    }
    
    res.send(idOrder.toString());
 })

async function flightSerach(kota_asal, kota_tujuan, tanggal_keberangkatan, tanggal_pulang, travelClass){
    console.log(kota_asal + " = " + kota_tujuan + " = " + tanggal_keberangkatan + "travelClass : " + travelClass)
    let t = await Flight.searchFlightModule(kota_asal, kota_tujuan,tanggal_keberangkatan, tanggal_pulang, undefined, travelClass)
    let ctr = 0;
    console.log("tanggal pulang" + tanggal_pulang)
    if (tanggal_pulang === undefined || tanggal_pulang.length == 0){
        let NewData = []
        for (let i = 0; i < t.length; i++) {
            t[i].Durasi = formatHelper.timeParse(t[i].Durasi)
            t[i].Jam_Berangkat = formatHelper.timeFormat(new Date(t[i].Jam_Berangkat))
            t[i].Jam_Sampai = formatHelper.timeFormat(new Date(t[i].Jam_Sampai))
            t[i].IDR = await Flight.convertToIdr(t[i].Harga, t[i].Curr)
            let fg = {
                "SearchId" : ctr,
                "Detail" : [t[i]]
            }
            NewData.push(fg)
            ctr++
        }
        t = NewData
        console.log("masuk");
    }
    else{
        let NewData = []
        for (let i = 0; i < t.length; i++) {
            for (let j = 0; j < t[i].length; j++) {
                t[i][j].Durasi = formatHelper.timeParse(t[i][j].Durasi)
                t[i][j].Jam_Berangkat = formatHelper.timeFormat(new Date(t[i][j].Jam_Berangkat))
                t[i][j].Jam_Sampai = formatHelper.timeFormat(new Date(t[i][j].Jam_Sampai))
                t[i][j].IDR = await Flight.convertToIdr(t[i][j].Harga, t[i][j].Curr)
            }
            let fg = {
                "SearchId" : ctr,
                "Detail" : t[i]
            }
            NewData.push(fg)
            ctr++
        }
        t = NewData
    }
    return t
}

async function checkToken(auth, action = "None"){
    let user = {};
    const token = auth
    if(!token){
        return {
            code : 401,
            message : "Token not found"
        }
    }
    try{
        user = jwt.verify(token,"travel");
        if((new Date().getTime()/1000)-user.iat>3600){    //60 menit
            return {
                code : 400,
                message : "Token expired"
            }
        }
        let login = await connection.query("SELECT * FROM user WHERE username = ?",[user.username])
        if (action != "reschedule" || (action == "reschedule" && user.subscribe == 1)){
            return {
                code : 200,
                message : "Valid Token Found",
                user : login[0],
            }
        }
        else{
            return {
                code : 400,
                message : "Not Authorized For this Action!, Only VIP User can access",
            }
        }
        
    }catch(err){
        //401 not authorized
        return {
            code : 401,
            message : "Token Invalid"
        }
    }
}

module.exports = router;
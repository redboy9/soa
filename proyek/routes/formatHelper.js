function timeFormat(element){
    element = new Date(element.getTime() - (element.getTimezoneOffset() * 60000)).toISOString().
    replace(/T/, ' ').      // replace T with a space
    replace(/\..+/, '')     // delete the dot and everything after
    return element
 }

 function rupiahFormat(element){
    var number_string = element.toString().replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah
    return 'Rp. ' + rupiah
 }

function timeParse (iso8601Duration) {
    let iso8601DurationRegex = /(-)?P(?:([.,\d]+)Y)?(?:([.,\d]+)M)?(?:([.,\d]+)W)?(?:([.,\d]+)D)?T(?:([.,\d]+)H)?(?:([.,\d]+)M)?(?:([.,\d]+)S)?/;
    let matches = iso8601Duration.match(iso8601DurationRegex);
    let durasi = (matches[6] === undefined ? "00:" : matches[6].length > 1 ?  matches[6] + ":" : "0" + matches[6] + ":") + (matches[7] === undefined ? "00:" : matches[7].length > 1 ?  matches[7] + ":" : "0"+matches[7] + ":") + ("00")
    return durasi
};

 function dateFormat(element){
    element = new Date(element);
    year = element.getFullYear();
    month = element.getMonth()+1;
    dt = element.getDate();
    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return year+'-' + month + '-'+dt
 }

 module.exports = {
     timeFormat : timeFormat,
     dateFormat : dateFormat,
     rupiahFormat : rupiahFormat,
     timeParse : timeParse,
 }
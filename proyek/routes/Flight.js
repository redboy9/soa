const express = require('express')
const connection = require("../connection")
const jwt = require('jsonwebtoken')
const request = require('request');
const router = express.Router()

const APIKEY_AMADEUS = 'VGxk29GRC8793xnumnaiBzuQh01tVFG5'
const APISECRET_AMADEUS = 'temI5MAfWkdhigKW'

var access_token = ''

async function getAccessToken(){
    await new Promise(function(resolve,reject){
        var authOptions = {
            url: 'https://test.api.amadeus.com/v1/security/oauth2/token',
            form: {
                grant_type : 'client_credentials',
                client_id : APIKEY_AMADEUS,
                client_secret : APISECRET_AMADEUS
            },
            json: true
        }
        request.post(authOptions, function(error, response, body) {
            if (!error && response.statusCode === 200) {
                access_token = body.access_token;
                resolve(access_token)
              }else{
               reject(new Erorr(error))
            }
        });
    })
}



router.get("/mostTraveled", async (req, res) => {
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
       return res.status(check.code).send({message : check.message})
    }
    let periode = req.body.periode;
    let kodeKota = req.body.kodeKota;
    await getAccessToken();
    const places = await new Promise(function(resolve,reject){
        var options = {
            'method' : 'GET',
            'url' : `https://test.api.amadeus.com/v1/travel/analytics/air-traffic/traveled?originCityCode=${kodeKota}&period=${periode}`,
            'headers' :{
                'Authorization': `Bearer ${access_token}`
            } 
        }
        request(options,function(error,response){
            if(error) reject(new Error(error));
            else resolve(JSON.parse(response.body));
        }); 
    })
    const data=[];
    if(places.meta.count == 0){
        return res.status(404).send({"message": `Penerbangan dari ${kodeKota} periode ${periode} tidak ditemukan`});
    }
    else{
        places.data.forEach(place => {
            data.push(place.destination);
        });
        return res.status(200).send(data);
    }
    
})

router.get("/searchFlight", async (req, res) => {
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
       return res.status(check.code).send({message : check.message})
    }
    let lokasiAsal = req.body.lokasiAsal;
    let lokasiDestinasi = req.body.lokasiDestinasi;
    let tanggalBerangkat = req.body.tanggalBerangkat;
    let tanggalPulang = req.body.tanggalPulang;
    let hargaMaksimal = req.body.hargaMaksimal;
    let mataUang = req.body.mataUang;
    let kelasTravel = req.body.kelasTravel;
    let url = `https://test.api.amadeus.com/v2/shopping/flight-offers?adults=1&originLocationCode=${lokasiAsal}&nonStop=true&destinationLocationCode=${lokasiDestinasi}&departureDate=${tanggalBerangkat}&travelClass=${kelasTravel}`;
    if(tanggalPulang != null && tanggalPulang.length > 0){
        url += `&returnDate=${tanggalPulang}`;
    }
    if(hargaMaksimal != null && hargaMaksimal.length > 0){
        url += `&maxPrice=${hargaMaksimal}`;
    }
    if(mataUang != null && mataUang.length > 0){
        url += `&currencyCode=${mataUang}`;
    }
    

    await getAccessToken();
    const flights = await new Promise(function(resolve,reject){
        var options = {
            'method' : 'GET',
            'url' : url,
            'headers' :{
                'Authorization': `Bearer ${access_token}`
            } 
        }
        request(options,function(error,response){
            if(error) reject(new Error(error));
            else resolve(JSON.parse(response.body));
        }); 
    })
    data = [];

    if(flights.meta.count == 0){
        return res.status(404).send({"message": "Tidak ditemukan penerbangan yang memenuhi kriteria"});
    }
    else{
        flights.data.forEach(flight => {
            let carrierCode =flight.itineraries[0].segments[0].carrierCode;
            let aircraftCode =flight.itineraries[0].segments[0].aircraft.code;
            let carrier = flights.dictionaries.carriers[carrierCode];
            let aircraft = flights.dictionaries.aircraft[aircraftCode];
            let PP = false;
            if(flight.itineraries.length >1) PP = true;
            if(PP){
                let carrierCode2 =flight.itineraries[1].segments[0].carrierCode;
                let aircraftCode2 =flight.itineraries[1].segments[0].aircraft.code;
                let carrier2 = flights.dictionaries.carriers[carrierCode2];
                let aircraft2 = flights.dictionaries.aircraft[aircraftCode2];
                dataFlight = [{
                    "Flight Number" : flight.itineraries[0].segments[0].number,
                    "Kode Pesawat" :  aircraftCode+ "-" + carrierCode,
                    "Nama Pesawat" : aircraft + "-" +carrier,
                    "Tempat Asal" : lokasiAsal, 
                    "Tempat Destinasi" : lokasiDestinasi, 
                    "Durasi" : flight.itineraries[0].duration.replace("PT",""),
                    "Jam Berangkat" : flight.itineraries[0].segments[0].departure.at.replace("T"," "),
                    "Jam Sampai" : flight.itineraries[0].segments[0].arrival.at.replace("T"," "),
                    "Harga" : flight.price.total + " " +flight.price.currency
                },
                {
                    "Flight Number" : flight.itineraries[1].segments[0].number,
                    "Kode Pesawat" :  aircraftCode2+ "-" + carrierCode2,
                    "Nama Pesawat" : aircraft2 + "-" +carrier2,
                    "Tempat Asal" : lokasiDestinasi, 
                    "Tempat Destinasi" : lokasiAsal, 
                    "Durasi" : flight.itineraries[0].duration.replace("PT",""),
                    "Jam Berangkat" : flight.itineraries[1].segments[0].departure.at.replace("T"," "),
                    "Jam Sampai" : flight.itineraries[1].segments[0].arrival.at.replace("T"," "),
                    "Harga" : flight.price.total + " " +flight.price.currency
                }
                ];
            }
            else {
                dataFlight = [{
                    "Flight Number" : flight.itineraries[0].segments[0].number,
                    "Kode Pesawat" :  aircraftCode+ "-" + carrierCode,
                    "Nama Pesawat" : aircraft + "-" +carrier,
                    "Tempat Asal" : lokasiAsal, 
                    "Tempat Destinasi" : lokasiDestinasi, 
                    "Durasi" : flight.itineraries[0].duration.replace("PT",""),
                    "Jam Berangkat" : flight.itineraries[0].segments[0].departure.at.replace("T"," "),
                    "Jam Sampai" : flight.itineraries[0].segments[0].arrival.at.replace("T"," "),
                    "Harga" : flight.price.total + " " +flight.price.currency
                }];
            }
            data.push(dataFlight);
        });
        
        return res.status(200).send(data);
    
    }    
})

router.get("/suggestHotel", async (req, res) => {
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
       return res.status(check.code).send({message : check.message})
    }
    let kodeKota = req.body.kodeKota;
    let tanggalCheckin = req.body.tanggalCheckin;
    let tanggalCheckout = req.body.tanggalCheckout;
    let jumlahOrang = req.body.jumlahOrang;
    let minimalRating = req.body.minimalRating;
    let fasilitas = req.body.fasilitas;
    let rating = "2,3,4,5";
    let url = `https://test.api.amadeus.com/v2/shopping/hotel-offers?adults=${jumlahOrang}&cityCode=${kodeKota}&radius=50&radiusUnit=KM&checkInDate=${tanggalCheckin}&checkOutDate=${tanggalCheckout}`;
    if(minimalRating != ""){
        rating = "";
        for(let i =minimalRating;i<6;i++){
            if(i == 5){
                rating += i;
            }
            else rating += i + ",";
        }
    }
    url += `&ratings=${rating}`;
    if (fasilitas != ""){
        url += `&amenities=${fasilitas}`;    
    }
    
    if(check.user.subscribe == 1){
        await getAccessToken();
        const hotels = await new Promise(function(resolve,reject){
            var options = {
                'method' : 'GET',
                'url' : url,
                'headers' :{
                    'Authorization': `Bearer ${access_token}`
                } 
            }
            request(options,function(error,response){
                if(error) reject(new Error(error));
                else resolve(JSON.parse(response.body));
            }); 
        })
        
        data = [];  
        if(hotels.data.length == 0){
            return res.status(404).send({"message":"Tidak ditemukan hotel yang memenuhi kriteria"});
        }
        else{
            hotels.data.forEach(hotelx => {
                let deskripsi= "Tidak ada deskripsi";
                if(hotelx.hotel.description != undefined) deskripsi = hotelx.hotel.description.text;
                dataHotel ={
                    "Nama Hotel": hotelx.hotel.name,
                    "Rating Hotel": hotelx.hotel.rating,
                    "Alamat Hotel": hotelx.hotel.address.lines[0],
                    "Nomor Telepon" : hotelx.hotel.contact.phone,
                    "Tanggal CheckIn" : hotelx.offers[0].checkInDate,
                    "Tanggal CheckOut" : hotelx.offers[0].checkOutDate,
                    "Deskripsi Hotel": deskripsi,
                    "Total harga": hotelx.offers[0].price.total + " " + hotelx.offers[0].price.currency
                }
                data.push(dataHotel);
            });
            return res.status(200).send(data);
        }
    }
    else res.status(400).send("Hanya VIP yang dapat mengakses");
})

function MinutesToDuration(s) {
    var days = Math.floor(s / 1440);
    s = s - days * 1440;
    var hours = Math.floor(s / 60);
    s = s - hours * 60;

    var dur = "PT";
    if (days > 0) {dur += days + "D"};
    if (hours > 0) {dur += hours + "H"};
    if (s > 0) dur += s + "M";

    return dur;
}


router.get("/delayPrediction/:flightID", async (req, res) => {
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
       return res.status(check.code).send({message : check.message})
    }
    if(check.user.subscribe == 1){
        let flightID = req.params.flightID;
        let idUser = check.user.id_user;
        await getAccessToken();
        let flight = await new Promise(function(resolve,reject){
            connection.query("SELECT * FROM flight_order WHERE id_user = ? and id_order = ?",[idUser, flightID],(err,rows)=>{
                if(err) reject(new Error(err));
                else if(rows.length ==0) res.status(404).send({"message":`Order dengan id ${flightID} tidak ditemukan`});
                else{
                    resolve(rows[0]);
                }
            })
        })
        
        let tempatAsal = flight.kota_asal;
        let tempatDestinasi = flight.kota_tujuan;
        let durasi = flight.duration;
        let splittedDurasi = durasi.split(":");
        durasi = (+splittedDurasi[0]) * 60 + (+splittedDurasi[1]);
        durasi = MinutesToDuration(durasi);
        let tanggalKeberangkatan = flight.tanggal_keberangkatan;
        tanggalKeberangkatan = tanggalKeberangkatan.toISOString().substr(0,10);
        let jamKeberangkatan = flight.jam_keberangkatan;
        let tanggalSampai = flight.tanggal_sampai;
        tanggalSampai = tanggalSampai.toISOString().substr(0,10);
        let jamSampai = flight.jam_sampai;
        let kodeAircraft = flight.aircraft_code;
        let kodeCarrier = flight.carrier_code;
        let flightNumber = flight.flight_number;
        const predictions = await new Promise(function(resolve,reject){
            var options = {
                'method' : 'GET',
                'url' : `https://test.api.amadeus.com/v1/travel/predictions/flight-delay?originLocationCode=${tempatAsal}&destinationLocationCode=${tempatDestinasi}&departureDate=${tanggalKeberangkatan}&departureTime=${jamKeberangkatan}&arrivalDate=${tanggalSampai}&arrivalTime=${jamSampai}&aircraftCode=${kodeAircraft}&carrierCode=${kodeCarrier}&flightNumber=${flightNumber}&duration=${durasi}`,
                'headers' :{
                    'Authorization': `Bearer ${access_token}`
                } 
            }
            request(options,function(error,response){
                if(error) reject(new Error(error));
                else resolve(JSON.parse(response.body));
            }); 
        })
        let data =[];
        let ctr=1;
        predictions.data.forEach(prediction => {
            let nama = "Prediksi delay kurang dari 30 menit";
            if(ctr == 2) nama = "Prediksi delay antara 30 sampai 60 menit";
            else if(ctr == 3) nama = "Prediksi delay antara 60 sampai 120 menit";
            else if(ctr == 4) nama = "Prediksi delay lebih dari 120 menit ato tercancel";
            isiData={
                "Prediksi" : nama,
                "Probabilitas" : (prediction.probability * 100) + "%"
            }
            ctr++;
            data.push(isiData);
        });
        
        return res.status(200).send(data);       
    }
    else return res.status(400).send("Hanya dapat diakses oleh VIP");
})



async function checkToken(auth){
    let user = {};
    const token = auth
    if(!token){
        return {
            code : 401,
            message : "Token not found"
        }
    }
    try{
        user = jwt.verify(token,"travel");
        if((new Date().getTime()/1000)-user.iat>3600){    //60 menit
            return {
                code : 400,
                message : "Token expired"
            }
        }
        let login = await connection.query("SELECT * FROM user WHERE username = ?",[user.username])
        return {
            code : 200,
            message : "Subscribe Success",
            user : login[0],
        }
        
    }catch(err){
        //401 not authorized
        return {
            code : 401,
            message : "Token Invalid"
        }
    }
}

module.exports = router
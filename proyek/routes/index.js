const express = require('express')
const connection = require("../connection")
const jwt = require('jsonwebtoken')
const multer = require('multer')
const request = require('request')
const router = express.Router()

const SERVERKEY_MIDTRANS = 'SB-Mid-server-DjSTXOi1HNwcWCDDRyYDsR1L'

const APIKEY_AMADEUS = 'VGxk29GRC8793xnumnaiBzuQh01tVFG5'
const APISECRET_AMADEUS = 'temI5MAfWkdhigKW'
var access_token = ''

async function getAccessToken(){
    var authOptions = {
        url: 'https://test.api.amadeus.com/v1/security/oauth2/token',
        form: {
            grant_type : 'client_credentials',
            client_id : APIKEY_AMADEUS,
            client_secret : APISECRET_AMADEUS
        },
        json: true
    }
    request.post(authOptions, function(error, response, body) {
		if (!error && response.statusCode === 200) {
			access_token = body.access_token;
            console.log(access_token)
	  	}else{
            console.log(error)
		}
    });
}

const storage = multer.diskStorage({
    destination : (req,file,callback) => {
        callback(null,'./uploads')
    },
    filename : (req,file,callback) => {
        const mime = file.originalname.split('.')
        callback(null, req.body.username ? req.body.username : file.originalname + '.' + mime[mime.length - 1])
    }
})
const upload = multer({
    storage : storage
})

router.post("/register", upload.single('fotoprofile'), async (req, res) => {
    let username = req.body.username
    let password = req.body.password
    let nama = req.body.nama
    let notelp = req.body.notelp
    let fotoprofile = req.file ? req.file.filename : 'blank.png'
    let cek = await connection.query("SELECT * FROM user WHERE username = ?",[username])
    if(cek.length == 0){
        await connection.query("INSERT INTO user VALUES(NULL, ?, ?, ?, ?, ?, '0')",[username, password, nama, notelp, fotoprofile])
        res.status(200).send({message : "Register Success"})
    }else{
        res.status(400).send({message : "Username already registered"})
    }
})

router.post("/login", async (req, res) => {
    let username = req.body.username
    let password = req.body.password
    let login = await connection.query("SELECT * FROM user WHERE username = ? AND password = ?",[username, password])
    if(login.length > 0){
        const token = jwt.sign({    
            username: login[0].username,
            subscribe : login[0].subscribe
        }, "travel");
        res.status(200).send({message : "Successfull create token", token : token});
    }else{
        res.status(400).send({message : "Username or password incorrect"})
    }
})
router.put("/editProfile", upload.single('fotoprofile'), async (req, res) => {
    let password = req.body.password
    let nama = req.body.nama
    let notelp = req.body.notelp
    let fotoprofile = req.file ? req.file.filename : ''
    let user = {};
    const token = req.header("x-auth-token");
    if(!token){
        res.status(401).send("Token not found");
    }
    try{
        user = jwt.verify(token,"travel");
        if((new Date().getTime()/1000)-user.iat>3600){    //60 menit
            return res.status(400).send("Token expired");
        }
        // if(user.subscribe == 0){
        //     return res.status(400).send("you are not allowed to access this resource")
        // }
    }catch(err){
        //401 not authorized
        return res.status(401).send("Token Invalid");
    }
    if(password && password != '') await connection.query("UPDATE user SET password = ? WHERE username = ?",[password, user.username])
    if(nama && nama != '') await connection.query("UPDATE user SET nama = ? WHERE username = ?",[nama, user.username])
    if(notelp && notelp != '') await connection.query("UPDATE user SET notelp = ? WHERE username = ?",[notelp, user.username])
    if(fotoprofile != '') await connection.query("UPDATE user SET fotoprofile = ? WHERE username = ?",[fotoprofile, user.username])
    return res.status(200).send("Update Success")
})
router.post("/subscribeAPI", async (req, res) => {
    let u = {};
    let user
    const token = req.header("x-auth-token");
    if(!token){
        res.status(401).send("Token not found");
    }
    try{
        u = jwt.verify(token,"travel");
        if((new Date().getTime()/1000)-u.iat>3600){    //60 menit
            return res.status(400).send("Token expired");
        }
        user = await connection.query("SELECT * FROM user WHERE username = ?",[u.username])
        if(user[0].subscribe == 1){
            return res.status(400).send("you are already subscribed")
        }
    }catch(err){
        //401 not authorized
        return res.status(401).send("Token Invalid");
    }

    
    let send = await new Promise((resolve,reject) => {
        var authOptions = {
            url: 'https://api.sandbox.midtrans.com/v2/charge',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization' : 'Basic ' + new Buffer(SERVERKEY_MIDTRANS).toString('base64')
            },
            body : {
                'payment_type' : 'bank_transfer',
                'transaction_details' : {
                    'gross_amount' : 150000,
                    'order_id' : 'order-'+user[0].id_user+'-'+Date.now()
                },
                'customer_details': {
                    'first_name' : user[0].nama,
                    'phone' : user[0].notelp
                },
                'item_details' : {
                    'price' : 150000,
                    'quantity' : 1,
                    'name' : 'Subscription API'
                },
                'bank_transfer' : {
                    'bank' : 'bca',
                    'va_number' : '12345678901',
                    'free_text' : {
                        'inquiry' : [
                            {
                                'id' : 'Your Custom Text in ID language',
                                'en' : 'Your Custom Text in EN language'
                            }
                        ],
                        'payment' : [
                            {
                                'id' : 'Your Custom Text in ID language',
                                'en' : 'Your Custom Text in EN language'
                            }
                        ]
                    }
                }
            },
            json: true,
        }
        request.post(authOptions, function(error, response, body) {
            if (error) {
                reject(new Error(error))
            }else{
                resolve(body)
            }
        });
    })
    // console.log(send)
    if(send.status_code == 201){
        res.status(200).send({
            'link' : 'https://simulator.sandbox.midtrans.com/assets/index.html',
            'order_id' : send.order_id,
            'Nomor VA' : send.va_numbers[0].va_number,
            'Nominal' : 'Rp. '+ send.gross_amount,
        })
    }else{
        res.status(200).send({
            'message' : 'Subscription failed'
        })
    }
})

module.exports = router;
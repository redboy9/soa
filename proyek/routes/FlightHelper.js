const express = require('express')
const connection = require("../connection")
const jwt = require('jsonwebtoken')
const request = require('request');
const router = express.Router()

const APIKEY_AMADEUS = 'VGxk29GRC8793xnumnaiBzuQh01tVFG5'
const APISECRET_AMADEUS = 'temI5MAfWkdhigKW'

var access_token = ''

async function getAccessToken(){
    await new Promise(function(resolve,reject){
        var authOptions = {
            url: 'https://test.api.amadeus.com/v1/security/oauth2/token',
            form: {
                grant_type : 'client_credentials',
                client_id : APIKEY_AMADEUS,
                client_secret : APISECRET_AMADEUS
            },
            json: true
        }
        request.post(authOptions, function(error, response, body) {
            if (!error && response.statusCode === 200) {
                access_token = body.access_token;
                resolve(access_token)
              }else{
               reject(new Error(error))
            }
        });
    })
}

async function searchFlightModule(lokasiAsal, lokasiDestinasi, tanggalBerangkat, tanggalPulang, hargaMaksimal, travelClass) {
    let url = `https://test.api.amadeus.com/v2/shopping/flight-offers?adults=1&originLocationCode=${lokasiAsal}&nonStop=true&destinationLocationCode=${lokasiDestinasi}&departureDate=${tanggalBerangkat}&travelClass=${travelClass}`;
    if(tanggalPulang != null && tanggalPulang.length > 0){
        url += `&returnDate=${tanggalPulang}`;
    }
    if(hargaMaksimal != null && hargaMaksimal.length > 0){
        url += `&maxPrice=${hargaMaksimal}`;
    }

    await getAccessToken();
    const flights = await new Promise(function(resolve,reject){
        var options = {
            'method' : 'GET',
            'url' : url,
            'headers' :{
                'Authorization': `Bearer ${access_token}`
            } 
        }
        request(options,function(error,response){
            if(error) reject(new Error(error));
            else resolve(JSON.parse(response.body));
        }); 
    })
    data = [];
    try {
        flights.data.forEach(flight => {
            let carrierCode =flight.itineraries[0].segments[0].carrierCode;
            let aircraftCode =flight.itineraries[0].segments[0].aircraft.code;
            let carrier = flights.dictionaries.carriers[carrierCode];
            let aircraft = flights.dictionaries.aircraft[aircraftCode];
            let PP = false;
            if(flight.itineraries.length >1) PP = true;
            if(PP){
                let carrierCode2 =flight.itineraries[1].segments[0].carrierCode;
                let aircraftCode2 =flight.itineraries[1].segments[0].aircraft.code;
                let carrier2 = flights.dictionaries.carriers[carrierCode2];
                let aircraft2 = flights.dictionaries.aircraft[aircraftCode2];
                dataFlight = [{
                    "Flight_Number" : flight.itineraries[0].segments[0].number,
                    "Aircraft_code" :  aircraftCode,
                    "Carrier_code" : carrierCode,
                    "Carrier" : carrier,
                    "Nama_Pesawat" : aircraft,
                    "Tempat_Asal" : lokasiAsal, 
                    "Tempat_Destinasi" : lokasiDestinasi, 
                    "Durasi" : flight.itineraries[0].duration,
                    "Jam_Berangkat" : flight.itineraries[0].segments[0].departure.at,
                    "Jam_Sampai" : flight.itineraries[0].segments[0].arrival.at,
                    "Harga" :flight.price.total,
                    "Curr" : flight.price.currency,
                    "Class" : travelClass,
                },
                {
                    "Flight_Number" : flight.itineraries[0].segments[0].number,
                    "Aircraft_code" :  aircraftCode2,
                    "Carrier_code" : carrierCode2,
                    "Carrier" : carrier2,
                    "Nama_Pesawat" : aircraft2,
                    "Tempat_Asal" : lokasiDestinasi, 
                    "Tempat_Destinasi" : lokasiAsal, 
                    "Durasi" : flight.itineraries[0].duration,
                    "Jam_Berangkat" : flight.itineraries[1].segments[0].departure.at,
                    "Jam_Sampai" : flight.itineraries[1].segments[0].arrival.at,
                    "Harga" :flight.price.total,
                    "Curr" : flight.price.currency,
                    "Class" : travelClass,
                }
                ];
            }
            else{
                dataFlight = {
                    "Flight_Number" : flight.itineraries[0].segments[0].number,
                    "Aircraft_code" :  aircraftCode,
                    "Carrier_code" : carrierCode,
                    "Carrier" : carrier,
                    "Nama_Pesawat" : aircraft,
                    "Tempat_Asal" : lokasiAsal, 
                    "Tempat_Destinasi" : lokasiDestinasi, 
                    "Durasi" : flight.itineraries[0].duration,
                    "Jam_Berangkat" : flight.itineraries[0].segments[0].departure.at,
                    "Jam_Sampai" : flight.itineraries[0].segments[0].arrival.at,
                    "Harga" :flight.price.total,
                    "Curr" : flight.price.currency,
                    "Class" : travelClass,
                };
            }
            data.push(dataFlight);
        });
    } catch (error) {
        
    }
    return data
}

async function convertToIdr(harga, currency){
    const curr = await new Promise(function(resolve,reject){
        var options = {
            'method' : 'GET',
            'url' : "https://api.exchangeratesapi.io/latest?base=" + currency,
        }
        request(options,function(error,response){
            if(error) reject(new Error(error));
            else resolve(JSON.parse(response.body));
        }); 
    })
    let hargaIDR = 0
    if(curr){
        hargaIDR = parseInt(harga) * parseInt(curr.rates.IDR)
    }
    return hargaIDR
}

module.exports = {
    searchFlightModule : searchFlightModule,
    convertToIdr : convertToIdr,
}
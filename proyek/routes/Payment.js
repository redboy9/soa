const express = require('express');
const midtransClient = require('midtrans-client');
const router = express.Router()
const connection = require("../connection")
const formatHelper = require("./formatHelper")
const jwt = require('jsonwebtoken')
/**
 * ===============
 * Using Snap API
 * ===============
 */

// Very simple Snap checkout
router.get('/simple_checkout/:idorder', async function (req, res) {
  // initialize snap client object
  let query = `SELECT * FROM flight_order WHERE ID_ORDER = '${req.params.idorder}'`;
  search_h = await connection.query(query, [])
  console.log(search_h.length)
  try {
    let snap = new midtransClient.Snap({
      isProduction : false,
      serverKey : 'SB-Mid-server-DjSTXOi1HNwcWCDDRyYDsR1L',
    clientKey : 'SB-Mid-client-Nl00phmUpgSIlsnk'
    });
    let parameter = {
      "transaction_details": {
        "order_id": "order-id-flight-"+ search_h[0].id_order,
        "gross_amount": search_h[0].total
      }, "credit_card":{
        "secure" : true
      }
    };
  
    for (let i = 0; i < search_h.length; i++) {
        search_h[i].tanggal_pemesanan = formatHelper.timeFormat(search_h[i].tanggal_pemesanan)
        search_h[i].tanggal_keberangkatan = formatHelper.dateFormat(search_h[i].tanggal_keberangkatan)
        search_h[i].tanggal_sampai = formatHelper.dateFormat(search_h[i].tanggal_sampai)
        search_h[i].total = formatHelper.rupiahFormat(search_h[i].total)
        let search_d = await connection.query("SELECT gelar, nama, if(tipe=1, 'Dewasa','Bayi') as keterangan FROM d_penumpang WHERE id_order = ?",[req.params.idorder])
        search_h[i].detail = search_d
    }
    console.log(search_h[0])
    // create snap transaction token
    try {
      await snap.createTransactionToken(parameter)
      .then((transactionToken)=>{
          // pass transaction token to frontend
          res.render('simple_checkout',{
            token: transactionToken, 
            clientKey: snap.apiConfig.clientKey,
            data : search_h[0]
          })
      })
    } catch (error) {
      res.render("NotFound")
    }
  } catch (error) {
    res.render("NotFound")
  }
})

/**
 * ===============
 * Using Core API - Credit Card
 * ===============
 */

// [0] Setup API client and config
let core = new midtransClient.CoreApi({
  isProduction : false,
  serverKey : 'SB-Mid-server-DjSTXOi1HNwcWCDDRyYDsR1L',
  clientKey : 'SB-Mid-client-Nl00phmUpgSIlsnk'
});

// [1] Render HTML+JS web page to get card token_id and [3] 3DS authentication
router.get('/simple_core_api_checkout', function (req, res) {
  res.render('simple_core_api_checkout',{ clientKey: core.apiConfig.clientKey })
})

// [2] Handle Core API credit card token_id charge
router.post('/charge_core_api_ajax', function (req, res) {
  console.log(`- Received charge request:`,req.body);
  core.charge({
    "payment_type": "credit_card",
    "transaction_details": {
      "gross_amount": 200000,
      "order_id": "order-id-node-"+Math.round((new Date()).getTime() / 1000),
    },
    "credit_card":{
      "token_id": req.body.token_id,
      "authentication": req.body.authenticate_3ds,
    }
  })
  .then((apiResponse)=>{
    res.send(`${JSON.stringify(apiResponse, null, 2)}`)
  })
  .catch((err)=>{
    res.send(`${JSON.stringify(err.ApiResponse, null, 2)}`)
  })
})


// [4] Handle Core API check transaction status
router.post('/check_transaction_status', async function(req, res){
  console.log(`- Received check transaction status request:`,req.body);
  let check = await checkToken(req.header("x-auth-token"));
  if(check.code != 200){
      return res.status(check.code).send({message : check.message})
  }
  let onlyOrderid = req.body.transaction_id
  let orderIdcheck = "order-id-flight-"+req.body.transaction_id
  try {
    await core.transaction.status(orderIdcheck)
    .then((transactionStatusObject)=>{
      let orderId = transactionStatusObject.order_id;
      let transactionStatus = transactionStatusObject.transaction_status;
      let fraudStatus = transactionStatusObject.fraud_status;

      let summary = `Transaction Result. Order ID: ${orderId}. Transaction status: ${transactionStatus}. Fraud status: ${fraudStatus}.<br>Raw transaction status:<pre>${JSON.stringify(transactionStatusObject, null, 2)}</pre>`;

      // [5.A] Handle transaction status on your backend
      // Sample transactionStatus handling logic
      if (transactionStatus == 'capture'){
          if (fraudStatus == 'challenge'){
              // TODO set transaction status on your databaase to 'challenge'
          } else if (fraudStatus == 'accept'){
              // TODO set transaction status on your databaase to 'success'
          }
      } else if (transactionStatus == 'settlement'){
        // TODO set transaction status on your databaase to 'success'
        // Note: Non card transaction will become 'settlement' on payment success
        // Credit card will also become 'settlement' D+1, which you can ignore
        // because most of the time 'capture' is enough to be considered as success
        let updateStatus = connection.query("update flight_order set status=3 where id_order = ?",[onlyOrderid])
      } else if (transactionStatus == 'cancel' ||
        transactionStatus == 'deny' ||
        transactionStatus == 'expire'){
        // TODO set transaction status on your databaase to 'failure'
      } else if (transactionStatus == 'pending'){
        // TODO set transaction status on your databaase to 'pending' / waiting payment
        let updateStatus = connection.query("update flight_order set status=2 where id_order = ?",[onlyOrderid])
      } else if (transactionStatus == 'refund'){
        // TODO set transaction status on your databaase to 'refund'
        let updateStatus = connection.query("update flight_order set status=-1 where id_order = ?",[onlyOrderid])
      }
      console.log(summary);
      res.json(transactionStatusObject);
    });
  } catch (error) {
    res.status(400).json({message : "transaction id not found!"})
  }
})

/**
 * ===============
 * Handling HTTP Post Notification
 * ===============
 */

router.post('/notification_handler', function(req, res){
  let receivedJson = req.body;
  core.transaction.notification(receivedJson)
    .then((transactionStatusObject)=>{
      let orderId = transactionStatusObject.order_id;
      let id_user = orderId.split('-')[1]
      let transactionStatus = transactionStatusObject.transaction_status;
      let fraudStatus = transactionStatusObject.fraud_status;

      let summary = `Transaction notification received. Order ID: ${orderId}. Transaction status: ${transactionStatus}. Fraud status: ${fraudStatus}.<br>Raw notification object:<pre>${JSON.stringify(transactionStatusObject, null, 2)}</pre>`;

      // [5.B] Handle transaction status on your backend via notification alternatively
      // Sample transactionStatus handling logic
      if (transactionStatus == 'capture'){
          if (fraudStatus == 'challenge'){
              // TODO set transaction status on your databaase to 'challenge'
          } else if (fraudStatus == 'accept'){
              // TODO set transaction status on your databaase to 'success'
          }
      } else if (transactionStatus == 'settlement'){
        connection.query("UPDATE user SET subscribe = '1' WHERE id_user = ?",[id_user])
        // TODO set transaction status on your databaase to 'success'
        // Note: Non-card transaction will become 'settlement' on payment success
        // Card transaction will also become 'settlement' D+1, which you can ignore
        // because most of the time 'capture' is enough to be considered as success
      } else if (transactionStatus == 'cancel' ||
        transactionStatus == 'deny' ||
        transactionStatus == 'expire'){
        // TODO set transaction status on your databaase to 'failure'
      } else if (transactionStatus == 'pending'){
        // TODO set transaction status on your databaase to 'pending' / waiting payment
      } else if (transactionStatus == 'refund'){
        // TODO set transaction status on your databaase to 'refund'
      }
      console.log(summary);
      res.send(summary);
    });
})

/**
 * ===============
 * Using Core API - other payment method, example: Permata VA
 * ===============
 */
router.get('/simple_core_api_checkout_permata', function (req, res) {
  console.log(`- Received charge request for Permata VA`);
  core.charge({
    "payment_type": "bank_transfer",
    "transaction_details": {
      "gross_amount": 200000,
      "order_id": "order-id-node-"+Math.round((new Date()).getTime() / 1000),
    }
  })
  .then((apiResponse)=>{
    res.render('simple_core_api_checkout_permata', {
      vaNumber: apiResponse.permata_va_number,
      amount: apiResponse.gross_amount,
      orderId: apiResponse.order_id
    });
  })
})

/**
 * ===============
 * Run Expressrouter
 * ===============
 */
// homepage
router.get('/', function (req, res) {
  res.render('index');
})
// credit card frontend demo
router.get('/core_api_credit_card_frontend_sample', function (req, res) {
  res.render('core_api_credit_card_frontend_sample',{ clientKey: core.apiConfig.clientKey });
})

async function checkToken(auth){
  let user = {};
  const token = auth
  if(!token){
      return {
          code : 401,
          message : "Token not found"
      }
  }
  try{
      user = jwt.verify(token,"travel");
      if((new Date().getTime()/1000)-user.iat>3600){    //60 menit
          return {
              code : 400,
              message : "Token expired"
          }
      }
     if(user.subscribe == 2){
          let login = await connection.query("SELECT * FROM user WHERE username = ?",[user.username])
          return {
              code : 200,
              message : "Subscribe Success",
              user : login[0],
          }
     }
     else{
          return {
              code : 400,
              message : "Not Authorized For this Action!",
          }
     }
      
  }catch(err){
      //401 not authorized
      return {
          code : 401,
          message : "Token Invalid"
      }
  }
}


module.exports = router
const express = require('express')
const connection = require("../connection")
const jwt = require('jsonwebtoken')
const multer = require('multer')
const router = express.Router()
const formatHelper = require("./formatHelper")
const APIKEY_AMADEUS = 'VGxk29GRC8793xnumnaiBzuQh01tVFG5'
const APISECRET_AMADEUS = 'temI5MAfWkdhigKW'

const storage = multer.diskStorage({
    destination : (req,file,callback) => {
        callback(null,'./uploads')
    },
    filename : (req,file,callback) => {
        const mime = file.originalname.split('.')
        callback(null, req.body.username ? req.body.username : file.originalname + '.' + mime[mime.length - 1])
    }
})
const upload = multer({
    storage : storage
})

router.get("/ListOrder", async (req, res) => {
   let idCustomer = req.params.idCustomer;
   let check = await checkToken(req.header("x-auth-token"));
   if(check.code != 200){
       return res.status(check.code).send({message : check.message})
   }
   let query = `SELECT id_order, id_user, tanggal_pemesanan, tanggal_keberangkatan, tanggal_sampai, kota_asal, kota_tujuan, total FROM flight_order WHERE 1 = 1`;
   req.query.id_order ? (query += ` AND ID_ORDER = '${req.query.id_order}'`):(``)
   req.query.id_user ? (query += ` AND ID_USER = '${req.query.id_user}'`):(``)
   req.query.min_date ? (query += ` AND Tanggal_pemesanan >= '${(req.query.min_date)}'`):(``)
   req.query.max_date ? (query += ` AND Tanggal_pemesanan <= '${(req.query.max_date)}'`):(``)
   search_h = await connection.query(query, [])
   if(search_h.length > 0){
        for (let i = 0; i < search_h.length; i++) {
            search_h[i].tanggal_pemesanan = formatHelper.timeFormat(search_h[i].tanggal_pemesanan)
            search_h[i].tanggal_keberangkatan = formatHelper.timeFormat(search_h[i].tanggal_keberangkatan)
            search_h[i].tanggal_sampai = formatHelper.timeFormat(search_h[i].tanggal_sampai)
        }
        return res.send(search_h)
   }
   else{
        return res.status(400).send({message : "Tidak ada list order sekarang ini"})
   }
})

router.get("/DetailOrder/:idOrder", async (req, res) => {
    let idOrder = req.params.idOrder;
    let check = await checkToken(req.header("x-auth-token"));
    if(check.code != 200){
        return res.status(check.code).send({message : check.message})
    }
    let search_h = await connection.query("SELECT * FROM flight_order WHERE id_order = ?",[idOrder])
    if(search_h.length>0){
        for (let i = 0; i < search_h.length; i++) {
            search_h[i].tanggal_pemesanan = formatHelper.timeFormat(search_h[i].tanggal_pemesanan)
            search_h[i].tanggal_keberangkatan = formatHelper.timeFormat(search_h[i].tanggal_keberangkatan)
            search_h[i].tanggal_sampai = formatHelper.timeFormat(search_h[i].tanggal_sampai)
            let search_d = await connection.query("SELECT gelar, nama, if(tipe=1, 'Dewasa','Bayi') as keterangan FROM d_penumpang WHERE id_order = ?",[idOrder])
            search_h[i].detail = search_d
        }
       return res.send(...search_h)
    }
    else{
        return res.status(400).send({message : "Detail Order tidak ditemukan"})
    }
 })

async function checkToken(auth){
    let user = {};
    const token = auth
    if(!token){
        return {
            code : 401,
            message : "Token not found"
        }
    }
    try{
        user = jwt.verify(token,"travel");
        if((new Date().getTime()/1000)-user.iat>3600){    //60 menit
            return {
                code : 400,
                message : "Token expired"
            }
        }
       if(user.subscribe == 2){
            let login = await connection.query("SELECT * FROM user WHERE username = ?",[user.username])
            return {
                code : 200,
                message : "Subscribe Success",
                user : login[0],
            }
       }
       else{
            return {
                code : 400,
                message : "Not Authorized For this Action!",
            }
       }
        
    }catch(err){
        //401 not authorized
        return {
            code : 401,
            message : "Token Invalid"
        }
    }
}

module.exports = router;
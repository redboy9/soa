-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Jun 2020 pada 14.25
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel`
--
CREATE DATABASE IF NOT EXISTS `travel` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `travel`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_penumpang`
--

DROP TABLE IF EXISTS `d_penumpang`;
CREATE TABLE `d_penumpang` (
  `id_order` varchar(10) NOT NULL,
  `gelar` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tipe` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `d_penumpang`
--

INSERT INTO `d_penumpang` (`id_order`, `gelar`, `nama`, `tipe`, `status`) VALUES
('1590028332', 'Mr', 'DDD', 1, 1),
('1590028332', 'Ms', 'Duck', 1, 1),
('1590125157', 'Mr', 'Wen', 1, 1),
('1590125157', 'Mr', 'Wen', 1, 1),
('1590125731', 'Mr', 'Wen', 1, 1),
('1590125731', 'Mr', 'Wen', 1, 1),
('1590125732', 'Ms', 'Duck', 0, 1),
('1590125732', 'Ms', 'Duck', 0, 1),
('1590125826', 'Mr', 'Wen', 1, 1),
('1590125826', 'Ms', 'Duck', 0, 1),
('1590125827', 'Mr', 'Wen', 1, 1),
('1590125827', 'Ms', 'Duck', 0, 1),
('1590125885', 'Mr', 'Wen', 1, 1),
('1590125885', 'Ms', 'Duck', 0, 1),
('1590125886', 'Mr', 'Wen', 1, 1),
('1590125886', 'Ms', 'Duck', 0, 1),
('1590125912', 'Mr', 'Wen', 1, 1),
('1590125912', 'Ms', 'Duck', 0, 1),
('1590204531', 'Mr', 'Wen', 1, 1),
('1590204531', 'Ms', 'Duck', 0, 1),
('1590204607', 'Mr', 'Wen', 1, 1),
('1590204607', 'Ms', 'Duck', 0, 1),
('1590206151', 'Mr', 'Wen', 1, 1),
('1590206151', 'Ms', 'Duck', 0, 1),
('1590206237', 'Mr', 'Wen', 1, 1),
('1590206237', 'Ms', 'Duck', 0, 1),
('1590465474', 'Mr', 'Wen', 1, 1),
('1590465474', 'Ms', 'Duck', 0, 1),
('1590465657', 'Mr', 'Wen', 1, 1),
('1590465657', 'Ms', 'Duck', 0, 1),
('1590465842', 'Mr', 'Wen', 1, 1),
('1590465842', 'Ms', 'Duck', 0, 1),
('1590480921', 'Mr', 'Wen', 1, 1),
('1590480921', 'Ms', 'Duck', 0, 1),
('1590480944', 'Mr', 'Wen', 1, 1),
('1590480944', 'Ms', 'Duck', 0, 1),
('1590481179', 'Mr', 'Wen', 1, 1),
('1590481179', 'Ms', 'Duck', 0, 1),
('1590481180', 'Mr', 'Wen', 1, 1),
('1590481180', 'Ms', 'Duck', 0, 1),
('1590481352', 'Mr', 'Wen', 1, 1),
('1590481352', 'Ms', 'Duck', 0, 1),
('1590487233', 'Mr', 'Wen', 1, 1),
('1590487233', 'Ms', 'Duck', 0, 1),
('1590487234', 'Mr', 'Wen', 1, 1),
('1590487234', 'Ms', 'Duck', 0, 1),
('1590487457', 'Mr', 'Wen', 1, 1),
('1590487457', 'Ms', 'Duck', 0, 1),
('1590487458', 'Mr', 'Wen', 1, 1),
('1590487458', 'Ms', 'Duck', 0, 1),
('1590487540', 'Mr', 'Wen', 1, 1),
('1590487540', 'Ms', 'Duck', 0, 1),
('1590487627', 'Mr', 'Wen', 1, 1),
('1590487627', 'Ms', 'Duck', 0, 1),
('1590487628', 'Mr', 'Wen', 1, 1),
('1590487628', 'Ms', 'Duck', 0, 1),
('1590487759', 'Mr', 'Wen', 1, 1),
('1590487759', 'Ms', 'Duck', 0, 1),
('1590487761', 'Mr', 'Wen', 1, 1),
('1590487761', 'Ms', 'Duck', 0, 1),
('1590488076', 'Mr', 'Wen', 1, 1),
('1590488076', 'Ms', 'Duck', 0, 1),
('1590488139', 'Mr', 'Wen', 1, 1),
('1590488139', 'Ms', 'Duck', 0, 1),
('1590488333', 'Mr', 'Wen', 1, 1),
('1590488333', 'Ms', 'Duck', 0, 1),
('1590488403', 'Mr', 'Wen', 1, 1),
('1590488403', 'Ms', 'Duck', 0, 1),
('1590488405', 'Mr', 'Wen', 1, 1),
('1590488405', 'Ms', 'Duck', 0, 1),
('1590488457', 'Mr', 'Wen', 1, 1),
('1590488457', 'Ms', 'Duck', 0, 1),
('1592223036', 'Mr', 'Wen', 1, 1),
('1592223036', 'Ms', 'Duck', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `flight_order`
--

DROP TABLE IF EXISTS `flight_order`;
CREATE TABLE `flight_order` (
  `id_order` varchar(10) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `tanggal_pemesanan` datetime NOT NULL,
  `carrier` varchar(50) NOT NULL,
  `aircraft_code` varchar(50) NOT NULL,
  `carrier_code` varchar(50) NOT NULL,
  `flight_number` varchar(50) NOT NULL,
  `duration` time NOT NULL,
  `kota_asal` varchar(50) NOT NULL,
  `kota_tujuan` varchar(50) NOT NULL,
  `jumlah_penumpang` int(11) NOT NULL,
  `tanggal_keberangkatan` date NOT NULL,
  `jam_keberangkatan` time NOT NULL,
  `tanggal_sampai` date NOT NULL,
  `jam_sampai` time NOT NULL,
  `class` varchar(50) NOT NULL,
  `total` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `flight_order`
--

INSERT INTO `flight_order` (`id_order`, `id_user`, `tanggal_pemesanan`, `carrier`, `aircraft_code`, `carrier_code`, `flight_number`, `duration`, `kota_asal`, `kota_tujuan`, `jumlah_penumpang`, `tanggal_keberangkatan`, `jam_keberangkatan`, `tanggal_sampai`, `jam_sampai`, `class`, `total`, `status`) VALUES
('1590028332', '2', '2020-05-21 05:24:18', 'Garuda Indonesia', 'AAAA', 'XCXC', '1234', '02:03:00', 'Sby', 'Mks', 2, '2020-05-29', '17:02:00', '2020-05-22', '29:10:00', 'ECONOMY', 5000000, 1),
('1590125157', '1', '2020-05-22 12:25:57', 'IBERIA', '32A', 'IB', '3445', '02:05:00', 'PAR', 'MAD', 2, '2020-05-25', '18:45:00', '2020-05-25', '20:50:00', 'ECONOMY', 2803465, 1),
('1590125731', '1', '2020-05-22 12:35:30', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '07:40:00', '2020-05-25', '09:40:00', 'ECONOMY', 1895985, 1),
('1590125732', '1', '2020-05-22 12:35:31', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'MAD', 'PAR', 2, '2020-05-25', '17:20:00', '2020-05-25', '19:15:00', 'ECONOMY', 1895985, 1),
('1590125826', '1', '2020-05-22 12:37:05', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '07:40:00', '2020-05-25', '09:40:00', 'ECONOMY', 1895985, 1),
('1590125827', '1', '2020-05-22 12:37:05', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'MAD', 'PAR', 2, '2020-05-25', '17:20:00', '2020-05-25', '19:15:00', 'ECONOMY', 1895985, 1),
('1590125885', '1', '2020-05-22 12:38:04', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '07:40:00', '2020-05-25', '09:40:00', 'ECONOMY', 1895985, 1),
('1590125886', '1', '2020-05-22 12:38:04', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'MAD', 'PAR', 2, '2020-05-25', '17:20:00', '2020-05-25', '19:15:00', 'ECONOMY', 1895985, 1),
('1590125912', '1', '2020-05-26 11:07:47', 'AIR EUROPA', '73H', 'UX', '1222', '02:00:00', 'PAR', 'MAD', 2, '2020-05-30', '17:10:00', '2020-05-30', '19:10:00', 'ECONOMY', 1203600, 1),
('1590204531', '1', '2020-05-23 10:28:51', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590204607', '1', '2020-05-23 10:30:06', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590205331', '1', '2020-05-23 10:42:10', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 31, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590205401', '1', '2020-05-23 10:43:20', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 31, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590205417', '1', '2020-05-23 10:43:36', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 31, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590206151', '1', '2020-05-23 10:55:51', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590206237', '1', '2020-05-23 10:57:17', 'AIR EUROPA', '73H', 'UX', '1026', '02:00:00', 'PAR', 'MAD', 2, '2020-05-25', '20:10:00', '2020-05-25', '22:10:00', 'ECONOMY', 1121181, 1),
('1590465474', '1', '2020-05-26 10:57:54', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590465657', '1', '2020-05-26 11:00:57', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590465842', '1', '2020-05-26 11:04:01', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590480921', '1', '2020-05-26 15:15:21', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590480944', '1', '2020-05-26 15:15:44', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590481179', '1', '2020-05-26 15:19:38', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-05-27', '10:05:00', '2020-05-27', '12:05:00', 'ECONOMY', 1685040, 1),
('1590481180', '1', '2020-05-26 15:19:38', 'AIR EUROPA', '788', 'UX', '1028', '02:00:00', 'MAD', 'PAR', 2, '2020-05-30', '17:20:00', '2020-05-30', '19:15:00', 'ECONOMY', 1685040, 1),
('1590481352', '1', '2020-05-26 15:22:32', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-08-01', '10:05:00', '2020-08-01', '12:05:00', 'ECONOMY', 2070192, 1),
('1590487233', '1', '2020-05-26 17:00:32', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-05-27', '10:05:00', '2020-05-27', '12:05:00', '', 1685040, -10),
('1590487234', '1', '2020-05-26 17:00:33', 'AIR EUROPA', '788', 'UX', '1028', '02:00:00', 'MAD', 'PAR', 2, '2020-05-30', '17:20:00', '2020-05-30', '19:15:00', '', 1685040, 1),
('1590487457', '1', '2020-05-26 17:04:16', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-05-27', '10:05:00', '2020-05-27', '12:05:00', 'ECONOMY', 1685040, 1),
('1590487458', '1', '2020-05-26 17:04:17', 'AIR EUROPA', '788', 'UX', '1028', '02:00:00', 'MAD', 'PAR', 2, '2020-05-30', '17:20:00', '2020-05-30', '19:15:00', 'ECONOMY', 1685040, 1),
('1590487540', '1', '2020-05-26 17:05:40', 'AIR EUROPA', '73H', 'UX', '1020', '02:00:00', 'PAR', 'MAD', 2, '2020-06-05', '07:40:00', '2020-06-05', '09:40:00', 'ECONOMY', 1107312, 1),
('1590487627', '1', '2020-05-26 17:07:06', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-05-27', '10:05:00', '2020-05-27', '12:05:00', 'ECONOMY', 1685040, 1),
('1590487628', '1', '2020-05-26 17:07:06', 'AIR EUROPA', '788', 'UX', '1028', '02:00:00', 'MAD', 'PAR', 2, '2020-05-30', '17:20:00', '2020-05-30', '19:15:00', 'ECONOMY', 1685040, 1),
('1590487759', '1', '2020-05-26 17:09:19', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-05-27', '10:05:00', '2020-05-27', '12:05:00', 'ECONOMY', 1685040, 1),
('1590487761', '1', '2020-05-26 17:09:19', 'AIR EUROPA', '788', 'UX', '1028', '02:00:00', 'MAD', 'PAR', 2, '2020-05-30', '17:20:00', '2020-05-30', '19:15:00', 'ECONOMY', 1685040, 1),
('1590488076', '1', '2020-05-26 17:14:36', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590488139', '1', '2020-05-26 17:15:39', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590488333', '1', '2020-05-26 17:18:52', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1590488403', '1', '2020-05-26 17:20:03', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-05-27', '10:05:00', '2020-05-27', '12:05:00', 'ECONOMY', 1685040, 1),
('1590488405', '1', '2020-05-26 17:20:03', 'AIR EUROPA', '788', 'UX', '1028', '02:00:00', 'MAD', 'PAR', 2, '2020-05-30', '17:20:00', '2020-05-30', '19:15:00', 'ECONOMY', 1685040, 1),
('1590488457', '1', '2020-05-26 17:20:56', 'IBERIA', '321', 'IB', '3417', '02:05:00', 'PAR', 'MAD', 2, '2020-05-27', '07:30:00', '2020-05-27', '09:35:00', 'ECONOMY', 1011024, 1),
('1592223036', '22', '2020-06-15 19:10:36', 'AIR EUROPA', '73H', 'UX', '1028', '02:00:00', 'PAR', 'MAD', 2, '2020-06-25', '10:05:00', '2020-06-25', '12:05:00', 'ECONOMY', 1589742, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `notelp` varchar(12) NOT NULL,
  `fotoprofile` varchar(255) NOT NULL,
  `subscribe` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama`, `notelp`, `fotoprofile`, `subscribe`) VALUES
(1, 'abc', '123', 'WEN', '12345', 'blank.png', 1),
(21, 'ADMIN', 'ADMIN', 'ADMIN', '', 'NO_PICT', 2),
(22, 'a', 'a', 'a', 'a', 'a', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `d_penumpang`
--
ALTER TABLE `d_penumpang`
  ADD KEY `id_order` (`id_order`);

--
-- Indeks untuk tabel `flight_order`
--
ALTER TABLE `flight_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `d_penumpang`
--
ALTER TABLE `d_penumpang`
  ADD CONSTRAINT `fk_order` FOREIGN KEY (`id_order`) REFERENCES `flight_order` (`id_order`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

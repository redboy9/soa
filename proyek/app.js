const express = require('express')
const indexRouter = require("./routes/index")
const OrderRouter = require("./routes/Order")
const AdminRouter = require("./routes/Admin")
const FlightRouter = require("./routes/Flight")
const PaymentRouter = require("./routes/Payment")
const port = process.env.PORT || 3000

let app = express();
app.set('views', 'views');
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use("/api/User", indexRouter);
app.use("/api/Order", OrderRouter);
app.use("/api/Admin/Order", AdminRouter);
app.use("/api/Flight",FlightRouter)
app.use("/api/Payment",PaymentRouter)

app.listen(port, () => {console.log(`Listening on ${port}...`)})